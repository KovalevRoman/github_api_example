from django.shortcuts import render
import requests
import json
from django.http import HttpResponse

def profile(request):
    parsedData = []
    if request.method == 'POST':
        username = request.POST.get('user')
        req = requests.get('https://api.github.com/users/' + username)
        jsonList = [ ]
        print(req.content)
        jsonList.append(req.json())
        userData = {}
        for data in jsonList:
            userData[ 'login' ] = data[ 'login' ]
            userData[ 'public_gists' ] = data[ 'public_gists' ]
            userData[ 'public_repos' ] = data[ 'public_repos' ]
            userData[ 'avatar_url' ] = data[ 'avatar_url' ]
            userData[ 'followers' ] = data[ 'followers' ]
            userData[ 'following' ] = data[ 'following' ]
            userData[ 'created_at' ] = data[ 'created_at' ]
            userData[ 'updated_at' ] = data[ 'updated_at' ]

        parsedData.append(userData)
    return render(request, 'profiles/profile.html', {'data': parsedData})
    # return HttpResponse(jsonList)