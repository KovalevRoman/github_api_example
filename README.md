# Github User data #

Пример работы с Github API с использованием Django - приложения

Пользователь может получить консолидированную информацию по введённому имени аккаунта на github.com

![Github API userdata](https://imageshack.com/a/img923/9958/YbnTuX.png)

### URL установленного проекта ###
http://81.2.248.93:8000/profile/

### Ключевые технологии ###

* [Django 1.11.6] (https://www.djangoproject.com)
* [Python Requests library 2.18.4] (http://docs.python-requests.org/en/master/)

### Кратко проекте ###

* Работа с формой (получение параметры из формы)
* Использование Function based view
* Отправка API запроса, получение и обработка ответа
* Передача контекста в html шаблон
* Получение и вывод контекста в клиенту (браузер)
* Deploy проекта на VPS Ubuntu, Nginx, Wsgi

### Контакты ###

* rvk.sft[ at ]gmail.com